package fr.marlou.imfine;

import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class APIRESTTest {
    @Test
    public void restOnline() throws IOException {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("51.178.143.181", 8080), 1000);
        }
    }
}