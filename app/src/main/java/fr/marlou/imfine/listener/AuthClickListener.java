package fr.marlou.imfine.listener;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import fr.marlou.imfine.AuthActivity;
import fr.marlou.imfine.MainActivity;
import fr.marlou.imfine.R;
import fr.marlou.imfine.service.key.KeyManager;
import fr.marlou.imfine.service.key.KeyResponse;
import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.service.task.AuthProfileTask;

public class AuthClickListener implements View.OnClickListener {

    private final AuthActivity activity;
    private final boolean login;

    public AuthClickListener(AuthActivity activity, boolean login) {
        this.activity = activity;
        this.login = login;
    }

    @Override
    public void onClick(View view) {
        EditText email = this.activity.findViewById(R.id.profile_email);
        EditText password = this.activity.findViewById(R.id.profile_password);

        // Si le mot de passe est invalide
        if(password.getText().length() != 4) {
            Toast.makeText(this.activity, this.activity.getResources().getString(R.string.invalid_password), Toast.LENGTH_LONG).show();
            return;
        }

        String emailString = email.getText().toString();
        // Si l'email est invalide
        if(emailString.isEmpty() || isEmailInvalid(emailString)) {
            Toast.makeText(this.activity, this.activity.getResources().getString(R.string.invalid_email), Toast.LENGTH_LONG).show();
            return;
        }

        // On crée un profil temporaire
        Profile profile = new Profile(emailString, password.getText().toString());

        try {
            // On envoie une requête à l'api
            KeyResponse keyResponse = new AuthProfileTask(error -> {
                this.activity.runOnUiThread(() -> this.activity.findViewById(R.id.message_red).setVisibility(View.VISIBLE));
            }, this.login).execute(profile).get();

            // Si on a une réponse
            if(keyResponse != null) {
                this.activity.findViewById(R.id.message_green).setVisibility(View.VISIBLE);

                // On insert la clé de connexion
                KeyManager.getInstance().insert(keyResponse);

                // On envoie l'utilisateur vers la page principale de l'application
                Intent intent = new Intent(this.activity, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.activity.startActivity(intent);
            }
        } catch (ExecutionException | InterruptedException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Vérifie avec du REGEX si l'eamil est invalide
     *
     * @param email
     * @return
     */
    private boolean isEmailInvalid(String email) {
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        return !pattern.matcher(email).matches();
    }
}
