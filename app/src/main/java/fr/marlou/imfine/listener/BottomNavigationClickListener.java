package fr.marlou.imfine.listener;

import android.content.Intent;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import fr.marlou.imfine.MainActivity;
import fr.marlou.imfine.ProfileActivity;
import fr.marlou.imfine.R;
import fr.marlou.imfine.SimulationActivity;

public class BottomNavigationClickListener implements NavigationBarView.OnItemSelectedListener {

    private final AppCompatActivity activity;
    private final int actualId;

    public BottomNavigationClickListener(BottomNavigationView navigationView, AppCompatActivity activity, int actualId) {
        this.activity = activity;
        this.actualId = actualId;

        navigationView.setSelectedItemId(actualId);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == actualId) return false;

        // Gère la  barre de navigation en bas de l'application
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(this.activity, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.activity.startActivity(intent);
                break;
            case R.id.party:
                this.activity.startActivity(new Intent(this.activity, SimulationActivity.class));
                break;
            case R.id.profile:
                this.activity.startActivity(new Intent(this.activity, ProfileActivity.class));
                break;
            default:
                return false;
        }
        return true;
    }
}
