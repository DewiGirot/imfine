package fr.marlou.imfine;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

import fr.marlou.imfine.listener.BottomNavigationClickListener;
import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.service.task.UpdateProfileTask;
import fr.marlou.imfine.util.MiddlewareUtils;

public class ProfileActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Profile profile;
        try {
            profile = MiddlewareUtils.redirect(this);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(this, "Middleware Error", Toast.LENGTH_LONG).show();
            return;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // On définie le titre de l'activité
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(this.getResources().getString(R.string.profile_page));

        BottomNavigationView navigationView = this.findViewById(R.id.BottomNavMenu);
        navigationView.setOnItemSelectedListener(new BottomNavigationClickListener(navigationView, this, R.id.profile));

        if(profile == null) return;

        /*
        On complète tous les champs
         */
        TextView nameView = findViewById(R.id.profile_name);
        if(profile.getName() != null) {
            nameView.setText(profile.getName());
        }

        TextView emailView = findViewById(R.id.profile_email);
        if(profile.getEmail() != null) {
            emailView.setText(profile.getEmail());
        }

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        radioGroup.check(profile.isMale() ? R.id.radioButton2 : R.id.radioButton);

        TextView weightView = findViewById(R.id.weight);
        if(profile.getWeight() != 0) {
            weightView.setText(String.valueOf(profile.getWeight()));
        }

        // On définie le listener appelé en cas de clique
        findViewById(R.id.button_update).setOnClickListener(view -> {
            Profile savedProfile = profile.clone();

            /*
            On complète le profil à sauvegardé en fonction des champs
             */
            String name = nameView.getText().toString();
            if(!name.isEmpty() && !name.equals(profile.getName())) {
                profile.setName(name);
            }

            String email = emailView.getText().toString();
            if(!email.isEmpty() && !email.equals(profile.getEmail())) {
                profile.setEmail(email);
            }

            String password = ((TextView) findViewById(R.id.profile_password)).getText().toString();
            if(!password.isEmpty()) {
                profile.setPassword(password);
            }

            String weightText = weightView.getText().toString();
            if(!weightText.isEmpty()) {
                profile.setWeight(Integer.parseInt(weightText));
            }

            profile.setMale(((RadioButton)findViewById(R.id.radioButton2)).isChecked());

            try {
                // Envoie une requête de mise à jour
                boolean affected = new UpdateProfileTask(error -> {
                    // todo: a clean
                    // On ne prend pas en compte les modifications car l'api a retourné une erreur
                    profile.setMale(savedProfile.isMale());
                    profile.setWeight(savedProfile.getWeight());
                    profile.setEmail(savedProfile.getEmail());
                    profile.setName(savedProfile.getName());

                    Toast.makeText(this, this.getResources().getString(R.string.profile_update_error), Toast.LENGTH_LONG).show();
                }).execute(profile).get();

                if(affected) {
                    Toast.makeText(this, this.getResources().getString(R.string.profile_update_success), Toast.LENGTH_LONG).show();
                } else {
                    // todo: a clean
                    // On ne prend pas en compte les modifications car il y a un problème interne à l'api
                    profile.setMale(savedProfile.isMale());
                    profile.setWeight(savedProfile.getWeight());
                    profile.setEmail(savedProfile.getEmail());
                    profile.setName(savedProfile.getName());

                    Toast.makeText(this, this.getResources().getString(R.string.profile_update_error), Toast.LENGTH_LONG).show();
                }

            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}