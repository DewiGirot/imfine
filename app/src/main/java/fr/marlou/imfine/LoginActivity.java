package fr.marlou.imfine;

import android.content.Intent;
import android.os.Bundle;

import java.util.Objects;

import fr.marlou.imfine.listener.AuthClickListener;

public class LoginActivity extends AuthActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // On définie le nom de l'activité
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(this.getResources().getString(R.string.connection));

        findViewById(R.id.button_login).setOnClickListener(new AuthClickListener(this, true));

        findViewById(R.id.no_account).setOnClickListener((view) -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            this.startActivity(intent);
        });
    }
}