package fr.marlou.imfine;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String lang = this.loadLocale();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        ActionBar actionBar = this.getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.settings_menu);
        }

        String[] langs = this.getResources().getStringArray(R.array.lang);
        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.auto_complete_lang);
        autoCompleteTextView.setText(langs[lang.equals("US") ? 1 : 0]);
        autoCompleteTextView.setOnItemClickListener((adapterView, view, i, l) -> {
            setLocale(i == 0 ? "FR" : "US");
            recreate(); // Relande l'activité
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getBaseContext(), R.layout.item_dropdown, langs);
        autoCompleteTextView.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish(); // Ferme l'activité
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sauvegarde la nouvelle langue
     *
     * @param localCode
     */
    public void setLocale(String localCode) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(localCode));
        res.updateConfiguration(conf, dm);
    }

    /**
     * @return la langue de l'application
     */
    private String loadLocale() {
        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        return conf.locale.getCountry();
    }
}
