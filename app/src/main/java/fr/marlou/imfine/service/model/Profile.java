package fr.marlou.imfine.service.model;

import java.util.List;

import fr.marlou.imfine.alcohol.AlcoholType;
import fr.marlou.imfine.alcohol.Consumption;

public class Profile implements Cloneable {

    private int id;
    private String email;
    private String password;
    private String name;
    private boolean male;
    private int weight;

    public Profile(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Profile() {
    }

    /**
     * @return vrai si toutes les informations importantes du profil sont complétés
     */
    public boolean isComplete() {
        return this.weight > 0;
    }

    /**
     * Calcule le taux d'alcoolémie d'un profil en fonction de plusieurs alcools
     *
     * @param consumptions
     * @return le taux d'alcoolémie
     */
    public double rateAlcoholism(List<Consumption> consumptions) {
        double val = 0;
        for (Consumption consumption : consumptions) {
            val += rateAlcoholism(consumption.getAlcoholType(), consumption.getGlasses());
        }
        return val;
    }

    /**
     * Calcule le taux d'alcoolémie d'un profil en fonction d'un alcool et de sa quantité
     *
     * @param alcoholType
     * @param quantity
     * @return le taux d'alcoolémie
     */
    public double rateAlcoholism(AlcoholType alcoholType, int quantity) {
        return ((alcoholType.getQuantity() * quantity) * alcoholType.getDegree() * 0.8) / ((this.male ? 0.7 : 0.6) * this.weight);
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public boolean isMale() {
        return male;
    }

    public int getWeight() {
        return weight;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public Profile clone() {
        try {
            return (Profile) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
