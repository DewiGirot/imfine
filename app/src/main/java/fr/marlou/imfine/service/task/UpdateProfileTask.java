package fr.marlou.imfine.service.task;

import fr.marlou.imfine.service.CustomAsyncTask;
import fr.marlou.imfine.service.error.APIErrorListener;
import fr.marlou.imfine.service.model.Profile;
import retrofit2.Response;

public class UpdateProfileTask extends CustomAsyncTask<Profile, Boolean> {

    public UpdateProfileTask(APIErrorListener listener) {
        super(listener);
    }

    @Override
    protected Boolean doInBackground(Profile... profiles) {
        // Envoie la requête de mise à jour du profil
        Response<Boolean> response = call(this.service.updateProfile(profiles[0]));
        return response == null ? null : response.body();
    }
}
