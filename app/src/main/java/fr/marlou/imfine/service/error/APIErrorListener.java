package fr.marlou.imfine.service.error;

/**
 * Interface fonctionnel pour écouter les erreurs renvoyées par l'api
 */
@FunctionalInterface
public interface APIErrorListener {

    APIErrorListener EMPTY = error -> {};

    void process(APIError error);
}
