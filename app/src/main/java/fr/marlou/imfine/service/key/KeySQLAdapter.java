package fr.marlou.imfine.service.key;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import fr.marlou.imfine.sql.SQLAdapter;

public class KeySQLAdapter extends SQLAdapter<KeyResponse, String> {


    /**
     * Insert une clé de connexion dans la table keys
     *
     * @param generic
     */
    @Override
    public void insert(KeyResponse generic) {
        ContentValues values = new ContentValues();
        values.put("keyS", generic.getKey());
        this.database.getWritableDatabase().insert("keys", null, values);
    }

    @Override
    public void update(KeyResponse genetic) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(KeyResponse generic) {
        throw new UnsupportedOperationException();
    }

    /**
     * @param generic
     * @return la dernière clé inséré dans la table keyS
     */
    @Override
    public KeyResponse find(String generic) {
        Cursor cursor = this.database.getReadableDatabase().query("keys", new String[] {"keyS", "creation"},
                null, null, null, null, "creation DESC");

        if (cursor.moveToNext()) {
            KeyResponse response = new KeyResponse(this.database.getString(cursor, "keyS"));
            cursor.close();
            return response;
        }

        cursor.close();
        return null;
    }

    @Override
    public List<KeyResponse> findAll() {
        throw new UnsupportedOperationException();
    }

    /**
     * Vide la table keys
     */
    @Override
    public void deleteAll() {
        this.database.getWritableDatabase().delete("keys", null, new String[0]);
    }
}
