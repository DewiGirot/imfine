package fr.marlou.imfine.service;

import fr.marlou.imfine.service.key.KeyResponse;
import fr.marlou.imfine.service.model.Profile;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Interface contenant toutes les routes de l'api
 */
public interface ProfileService {

    @GET("profile")
    Call<Profile> getProfile();

    @POST("register")
    Call<KeyResponse> register(@Body Profile profile);

    @POST("login")
    Call<KeyResponse> login(@Body Profile profile);

    @POST("profile")
    Call<Boolean> updateProfile(@Body Profile profile);

}
