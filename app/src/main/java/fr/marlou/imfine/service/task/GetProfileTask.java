package fr.marlou.imfine.service.task;

import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.service.CustomAsyncTask;
import fr.marlou.imfine.service.error.APIErrorListener;
import retrofit2.Response;

public class GetProfileTask extends CustomAsyncTask<Void, Profile> {

    public GetProfileTask(APIErrorListener listener) {
        super(listener);
    }

    @Override
    protected Profile doInBackground(Void... voids) {
        // Récupère le profil associé à sa clé
        Response<Profile> response = call(this.service.getProfile());
        return response == null ? null : response.body();
    }
}



