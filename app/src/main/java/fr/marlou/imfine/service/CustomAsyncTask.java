package fr.marlou.imfine.service;

import android.os.AsyncTask;

import java.io.IOException;
import java.lang.annotation.Annotation;

import fr.marlou.imfine.service.error.APIError;
import fr.marlou.imfine.service.error.APIErrorListener;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class CustomAsyncTask<P, T> extends AsyncTask<P, Void, T> {

    private final APIErrorListener listener;
    protected Retrofit retrofit;
    protected ProfileService service;

    public CustomAsyncTask(APIErrorListener listener) {
        this.listener = listener;

        // Création du client
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new AuthorizationInterceptor());

        // Initialization de retrofit
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://51.178.143.181:8080/") // api.imfine.killian-dev.fr - Pas de NDD pour éviter les pb de DNS
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        // Initialization de ProfileService
        this.service = retrofit.create(ProfileService.class);
    }

    /**
     * Execute une route de ProfileService
     *
     * @param call
     * @return la réponse
     */
    protected Response<T> call(Call<T> call) {
        try {
            Response<T> response = call.execute();

            // Si l'api retourne une erreur
            if(!response.isSuccessful()) {
                // On appelle le listener
                this.listener.process(parseError(response.errorBody()));
                return null;
            }

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Générer une instance de APIError en fonction de l'erreur retourné par l'api
     *
     * @param response
     * @return une instance de APIError
     */
    private APIError parseError(ResponseBody response) {
        Converter<ResponseBody, APIError> converter = this.retrofit.responseBodyConverter(APIError.class, new Annotation[0]);

        try {
            return converter.convert(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}