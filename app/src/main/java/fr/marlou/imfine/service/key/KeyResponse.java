package fr.marlou.imfine.service.key;

public class KeyResponse {

    private String key;

    public KeyResponse(String key) {
        this.key = key;
    }

    public KeyResponse() {
    }

    public String getKey() {
        return key;
    }
}
