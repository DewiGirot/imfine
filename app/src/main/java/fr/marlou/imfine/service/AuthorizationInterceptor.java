package fr.marlou.imfine.service;

import java.io.IOException;

import fr.marlou.imfine.service.key.KeyManager;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        // Ajout de la clé de connexion dans le header de la requête
        Request request = original.newBuilder()
                .header("Authorization", "Bearer " + KeyManager.getInstance().getActualKeyOrEmpty())
                .method(original.method(), original.body())
                .build();

        return chain.proceed(request);
    }
}
