package fr.marlou.imfine.service.error;

/**
 * Classe modélisant une erreur de l'api
 */
public class APIError {

    private int status;
    private String error;
    private String message;

    public APIError() {
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
