package fr.marlou.imfine.service.key;

public class KeyManager {

    private static KeyManager instance;

    private final KeySQLAdapter sqlAdapter;
    private KeyResponse actualKey;

    private KeyManager() {
        this.sqlAdapter = new KeySQLAdapter();
        this.actualKey = this.sqlAdapter.find(null);;
    }

    /**
     * @return la dernière clé de connexion
     */
    public KeyResponse getActualKey() {
        return this.actualKey;
    }

    /**
     * @return la dernière clé de connexion ou un String vide
     */
    public String getActualKeyOrEmpty() {
        return actualKey == null ? "" : this.actualKey.getKey();
    }

    /**
     * Insert une clé de connexion
     *
     * @param keyResponse
     */
    public void insert(KeyResponse keyResponse) {
        this.sqlAdapter.insert(keyResponse);
        this.actualKey = keyResponse;
    }

    /**
     * Vide toutes les clés de connexion
     */
    public void disconnect() {
        this.sqlAdapter.deleteAll();
        this.actualKey = null;
    }

    /**
     * @return l'instance du sigleton
     */
    public static KeyManager getInstance() {
        return instance == null ? instance = new KeyManager() : instance;
    }
}
