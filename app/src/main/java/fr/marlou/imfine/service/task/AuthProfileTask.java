package fr.marlou.imfine.service.task;

import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.service.CustomAsyncTask;
import fr.marlou.imfine.service.key.KeyResponse;
import fr.marlou.imfine.service.error.APIErrorListener;
import retrofit2.Response;

public class AuthProfileTask extends CustomAsyncTask<Profile, KeyResponse> {

    private final boolean login;

    public AuthProfileTask(APIErrorListener listener, boolean login) {
        super(listener);
        this.login = login;
    }

    @Override
    protected KeyResponse doInBackground(Profile... profiles) {
        // Envoie une requête de connexion/inscription à l'api
        Response<KeyResponse> response = call(this.login ? this.service.login(profiles[0]) :
                this.service.register(profiles[0]));
        return response == null ? null : response.body();
    }
}



