package fr.marlou.imfine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import fr.marlou.imfine.alcohol.AlcoholManager;
import fr.marlou.imfine.alcohol.Consumption;

public class AlcoholDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int index = this.getIntent().getIntExtra("index", -1);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alcohol_detail);

        ActionBar actionBar = this.getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.simulation_page);
        }

        if(index == -1) return;

        Consumption consumption = AlcoholManager.getInstance().getCache().get(index);

        EditText editText = findViewById(R.id.glasses_number);
        editText.setText(String.valueOf(consumption.getGlasses()));

        // Si il veut supprimer la Consumption on ferme l'activité avec un résultat positif + les informations dans l'intent
        findViewById(R.id.button_delete).setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.putExtra("index", index);
            intent.putExtra("delete", true);
            this.setIntent(intent);
            setResult(Activity.RESULT_OK, intent);
            finish();
        });

        // Si il veut modifier la Consumption on ferme l'activité avec un résultat positif + les informations dans l'intent
        findViewById(R.id.button_submit).setOnClickListener(view -> {
            String glassesString = editText.getText().toString();
            if(glassesString.isEmpty()) return;

            Intent intent = new Intent();
            intent.putExtra("index", index);
            intent.putExtra("delete", false);
            intent.putExtra("glasses", Integer.parseInt(glassesString));
            this.setIntent(intent);
            setResult(Activity.RESULT_OK, intent);
            finish();
        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish(); // On ferme l'activité dans résultat positif
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}