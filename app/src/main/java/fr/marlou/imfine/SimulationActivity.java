package fr.marlou.imfine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

import fr.marlou.imfine.alcohol.AlcoholAdapter;
import fr.marlou.imfine.alcohol.AlcoholManager;
import fr.marlou.imfine.alcohol.AlcoholType;
import fr.marlou.imfine.alcohol.Consumption;
import fr.marlou.imfine.listener.BottomNavigationClickListener;
import fr.marlou.imfine.popup.help.PopupHelp;

public class SimulationActivity extends CustomActivity {

    private PopupHelp popupHelp;
    private AlcoholAdapter alcoholAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation);

        Objects.requireNonNull(this.getSupportActionBar()).setTitle(this.getResources().getString(R.string.simulation_page));

        BottomNavigationView navigationView = this.findViewById(R.id.BottomNavMenu);
        navigationView.setOnItemSelectedListener(new BottomNavigationClickListener(navigationView, this, R.id.party));

        this.popupHelp = new PopupHelp(this);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getBaseContext(), R.layout.item_dropdown,
                this.getResources().getStringArray(R.array.alcohols));
        ((AutoCompleteTextView)findViewById(R.id.auto_complete_alcohol)).setAdapter(arrayAdapter);

        ListView alcoholListView = findViewById(R.id.listview);
        alcoholListView.setAdapter(this.alcoholAdapter = new AlcoholAdapter(this, this));

        findViewById(R.id.button_submit).setOnClickListener(view -> {
            String glassesString = ((TextView) findViewById(R.id.glasses_number)).getText().toString();
            if(glassesString.isEmpty()) return;

            String alcohol = ((AutoCompleteTextView) findViewById(R.id.auto_complete_alcohol)).getText().toString();
            if(alcohol.isEmpty()) return;

            AlcoholType alcoholType = AlcoholType.getAlcohol(alcohol);
            if(alcoholType == null) return;

            int glasses = Integer.parseInt(glassesString);

            AlcoholManager manager = AlcoholManager.getInstance();
            manager.getSqlAdapter().insert(new Consumption(-1, alcoholType, glasses));
            manager.updateCache();
            this.alcoholAdapter.notifyDataSetChanged();
        });

        findViewById(R.id.button_clear).setOnClickListener(view -> {
            AlcoholManager manager = AlcoholManager.getInstance();
            manager.getSqlAdapter().deleteAll();
            manager.updateCache();
            this.alcoholAdapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == Activity.RESULT_OK && data != null) { // On traite les données renvoyé par AlcoholDetailActivity
            boolean delete = data.getBooleanExtra("delete", true);
            int index = data.getIntExtra("index", -1);

            if(delete) {
                AlcoholManager.getInstance().delete(index);
            } else {
                AlcoholManager.getInstance().update(index, data.getIntExtra("glasses", 1));
            }
            this.alcoholAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_simulation, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.help) {
            this.popupHelp.show(); // Affiche le popup
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}