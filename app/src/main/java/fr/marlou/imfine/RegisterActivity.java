package fr.marlou.imfine;

import android.os.Bundle;

import java.util.Objects;

import fr.marlou.imfine.listener.AuthClickListener;

public class RegisterActivity extends AuthActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // On définie le titre de l'activité
        Objects.requireNonNull(this.getSupportActionBar()).setTitle(this.getResources().getString(R.string.register_page));

        findViewById(R.id.button_register).setOnClickListener(new AuthClickListener(this, false));
    }
}