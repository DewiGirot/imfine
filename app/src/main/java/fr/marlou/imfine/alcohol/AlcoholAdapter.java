package fr.marlou.imfine.alcohol;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import fr.marlou.imfine.AlcoholDetailActivity;
import fr.marlou.imfine.R;
import fr.marlou.imfine.SimulationActivity;

public class AlcoholAdapter extends BaseAdapter {

    private final SimulationActivity simulationActivity;
    private final LayoutInflater inflater;

    public AlcoholAdapter(SimulationActivity simulationActivity, Context context) {
        this.simulationActivity = simulationActivity;
        this.inflater = LayoutInflater.from(context);
    }

    /**
     * @return le nombre d'elements dans l'adapteur
     */
    @Override
    public int getCount() {
        return AlcoholManager.getInstance().getCache().size();
    }

    /**
     * Retourne un item d'incice i
     *
     * @param i indice de l'item
     * @return un item
     */
    @Override
    public Object getItem(int i) {
        return AlcoholManager.getInstance().getCache().get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((Consumption) this.getItem(i)).getId();
    }

    /**
     * Returne et créer une AlcoholView
     *
     * @param i indice de l'item
     * @param view vue qui va être créé/mise à jour
     * @param viewGroup
     * @return une vue
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AlcoholView alcoholView = new AlcoholView();
        if(view == null) { // On crée la vue
            view = this.inflater.inflate(R.layout.item_alcohol, null);
            alcoholView.alcoholView = view.findViewById(R.id.alcohol_name);
            alcoholView.quantityView = view.findViewById(R.id.alcohol_quantity);
            alcoholView.cardView = view.findViewById(R.id.card_view);
            view.setTag(alcoholView);
        } else { // On récupère la vue
            alcoholView = (AlcoholView) view.getTag();
        }

        // On complete les éléments
        Consumption consumption = (Consumption) this.getItem(i);
        alcoholView.alcoholView.setText(consumption.getAlcoholType().getName());
        alcoholView.quantityView.setText(view.getContext().getResources()
                .getString(R.string.glasses_consumption).replace("{nb}", String.valueOf(consumption.getGlasses())));

        alcoholView.cardView.setOnClickListener(view1 -> {
            Intent intent = new Intent(this.simulationActivity, AlcoholDetailActivity.class);
            intent.putExtra("index", i);
            this.simulationActivity.startActivityForResult(intent, 1);
        });

        return view;
    }

    /**
     * Vue de l'item alcohol
     */
    static class AlcoholView {
        public TextView alcoholView;
        public TextView quantityView;
        public CardView cardView;
    }
}
