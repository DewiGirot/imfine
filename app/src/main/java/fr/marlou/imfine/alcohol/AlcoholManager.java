package fr.marlou.imfine.alcohol;

import java.util.ArrayList;
import java.util.List;

public class AlcoholManager {

    private static AlcoholManager instance;

    private final AlcoholSQLAdapter sqlAdapter;
    private List<Consumption> cache = new ArrayList<>();

    private AlcoholManager() {
        this.sqlAdapter = new AlcoholSQLAdapter();
        this.updateCache();
    }

    /**
     * @return l'adateur sql
     */
    public AlcoholSQLAdapter getSqlAdapter() {
        return sqlAdapter;
    }

    /**
     * Supprime une Consumption d'incice index
     *
     * @param index
     */
    public void delete(int index) {
        Consumption consumption = this.cache.get(index);
        this.sqlAdapter.delete(consumption);
        this.updateCache();
    }

    /**
     * Met à jour une Consumption d'incice index
     *
     * @param index
     * @param glasses
     */
    public void update(int index, int glasses) {
        Consumption consumption = this.cache.get(index);
        consumption.setGlasses(glasses);
        this.sqlAdapter.update(consumption);
        this.updateCache();
    }

    /**
     * Met à jour le cache avec la base de données
     */
    public void updateCache() {
        this.cache = this.sqlAdapter.findAll();
    }

    /**
     * @return le cache
     */
    public List<Consumption> getCache() {
        return cache;
    }

    /**
     * @return l'instance du sigleton
     */
    public static AlcoholManager getInstance() {
        return instance == null ? instance = new AlcoholManager() : instance;
    }
}
