package fr.marlou.imfine.alcohol;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import fr.marlou.imfine.sql.SQLAdapter;

public class AlcoholSQLAdapter extends SQLAdapter<Consumption, Integer> {

    /**
     * Insert une Consumption dans la table consumptions
     *
     * @param generic
     */
    @Override
    public void insert(Consumption generic) {
        ContentValues values = new ContentValues();
        values.put("alcohol", generic.getAlcoholType().name());
        values.put("glasses", generic.getGlasses());
        this.database.getWritableDatabase().insert("consumptions", null, values);
    }

    /**
     * Mettre à jour une Consumption dans la table consumptions
     *
     * @param genetic
     */
    @Override
    public void update(Consumption genetic) {
        ContentValues values = new ContentValues();
        values.put("glasses", genetic.getGlasses());
        this.database.getWritableDatabase().update("consumptions", values, "idConsumption = ?",
                new String[] {String.valueOf(genetic.getId())});
    }

    /**
     * Supprimer une Consumption dans la table consumptions
     *
     * @param generic
     */
    @Override
    public void delete(Consumption generic) {
        this.database.getWritableDatabase().delete("consumptions", "idConsumption = ?",
                new String[] {String.valueOf(generic.getId())});
    }

    @Override
    public Consumption find(Integer generic) {
        throw new UnsupportedOperationException();
    }

    /**
     * @return la liste de toutes les les consumptions dans la table consumptions
     */
    @Override
    public List<Consumption> findAll() {
        Cursor cursor = this.database.getReadableDatabase().query("consumptions", new String[] {"idConsumption", "alcohol", "glasses"},
                null, null, null, null, null);


        List<Consumption> consumptions = new ArrayList<>();
        while (cursor.moveToNext()) {
            Consumption consumption = new Consumption(this.database.getInt(cursor, "idConsumption"), AlcoholType.valueOf(this.database.getString(cursor, "alcohol")),
                    this.database.getInt(cursor, "glasses"));
            consumptions.add(consumption);
        }

        cursor.close();
        return consumptions;
    }

    /**
     * Vide la table consumptions
     */
    @Override
    public void deleteAll() {
        this.database.getWritableDatabase().delete("consumptions", null, new String[0]);
    }
}
