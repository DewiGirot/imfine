package fr.marlou.imfine.alcohol;

public class Consumption {

    private final int id;
    private final AlcoholType alcoholType;
    private int glasses;

    public Consumption(int id, AlcoholType alcoholType, int glasses) {
        this.id = id;
        this.alcoholType = alcoholType;
        this.glasses = glasses;
    }

    public int getId() {
        return id;
    }

    public AlcoholType getAlcoholType() {
        return alcoholType;
    }

    public int getGlasses() {
        return glasses;
    }

    public void setGlasses(int glasses) {
        this.glasses = glasses;
    }
}
