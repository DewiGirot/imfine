package fr.marlou.imfine.alcohol;

public enum AlcoholType {

    VIN("Vin", 100, 0.12),
    BIERE("Bière", 250, 0.05),
    WHISKY("Whisky", 30, 0.40),
    RHUM("Rhum", 30, 0.40),
    TEQUILA("Tequila", 30, 0.40),
    VODKA("Vodka", 30, 0.40),
    JAGERMAEISTER("Jägermaeister", 30, 0.40),
    PASTIS("Pastis", 20, 0.45),
    CHAMPAGNE("Champagne", 125, 0.11),
    ;

    private final String name;
    private final int quantity;
    private final double degree;

    AlcoholType(String name, int quantity, double degree) {
        this.name = name;
        this.quantity = quantity;
        this.degree = degree;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getDegree() {
        return degree;
    }

    /**
     * Retourne un AlcoholType en fonction de son nom
     *
     * @param name nom de l'alcool
     * @return un AlcoholType
     */
    public static AlcoholType getAlcohol(String name) {
        for (AlcoholType type : values()) {
            if(type.name.equalsIgnoreCase(name))
                return type;
        }
        return null;
    }
}
