package fr.marlou.imfine;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

import fr.marlou.imfine.service.key.KeyManager;
import fr.marlou.imfine.util.MiddlewareUtils;

/**
 * Classe qui intègre automatiquement la gestion du menu dans l'action bar
 */
public class CustomActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.settings) {
            this.startActivity(new Intent(this, SettingActivity.class));
            return true;
        } else if(item.getItemId() == R.id.disconnect) {
            KeyManager.getInstance().disconnect(); // On déconnecte l'utilisateur
            try {
                MiddlewareUtils.redirect(this); // On l'envoie vers la page de connexion
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
