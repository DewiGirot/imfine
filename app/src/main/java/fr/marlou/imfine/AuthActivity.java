package fr.marlou.imfine;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

import fr.marlou.imfine.util.MiddlewareUtils;

/**
 * Classe pour intégrer automatiquement le MiddlewareUtils à LoginActivity & RegisterActivity
 */
public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            MiddlewareUtils.redirect(this);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
    }
}
