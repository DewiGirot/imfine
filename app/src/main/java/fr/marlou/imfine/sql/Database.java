package fr.marlou.imfine.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    
    public Database(Context context) {
        super(context, "imfine.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        // Création des tables
        database.execSQL("CREATE TABLE keys(keyS TEXT NOT NULL PRIMARY KEY, creation LONG DEFAULT CURRENT_TIMESTAMP);");
        database.execSQL("CREATE TABLE consumptions(idConsumption INTEGER PRIMARY KEY AUTOINCREMENT, alcohol VARCHAR(255), glasses INT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Retourne la valeur de la colonne key du curseur cursor
     *
     * @param cursor
     * @param key
     * @return un string
     */
    public String getString(Cursor cursor, String key) {
        return cursor.getString(cursor.getColumnIndexOrThrow(key));
    }

    /**
     * Retourne la valeur de la colonne key du curseur cursor
     *
     * @param cursor
     * @param key
     * @return un nombre
     */
    public int getInt(Cursor cursor, String key) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(key));
    }
}
