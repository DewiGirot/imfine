package fr.marlou.imfine.sql;

import android.content.Context;

import androidx.annotation.Nullable;

public class SQLManager {

    private static SQLManager instance;

    private final Database database;

    private SQLManager(Context context) {
        this.database = new Database(context);
    }

    /**
     * @return la base de données
     */
    public Database getDatabase() {
        return database;
    }

    /**
     * @param context pour intialisé la base de données si elle est null
     * @return l'instance du sigleton
     */
    public static SQLManager getInstance(Context context) {
        return instance == null ? instance = new SQLManager(context) : instance;
    }

    /**
     * @return l'instance du sigleton
     */
    @Nullable
    public static SQLManager getInstance() {
        return instance;
    }
}
