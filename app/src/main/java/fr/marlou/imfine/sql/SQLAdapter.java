package fr.marlou.imfine.sql;

import java.util.List;

public abstract class SQLAdapter<T, K> {

    protected Database database;

    public SQLAdapter() {
        SQLManager manager = SQLManager.getInstance();
        if (manager == null) {
            throw new RuntimeException("SQLManager null");
        }
        this.database = manager.getDatabase();
    }

    public abstract void insert(T generic);

    public abstract void update(T genetic);

    public abstract void delete(T generic);

    public abstract T find(K generic);

    public abstract List<T> findAll();

    public abstract void deleteAll();

}
