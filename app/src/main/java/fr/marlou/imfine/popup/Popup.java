package fr.marlou.imfine.popup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public abstract class Popup<A extends AppCompatActivity> {

    protected final A activity;

    protected AlertDialog dialog;

    public Popup(A activity, int resource) {
        this.activity = activity;

        // Crée le popup
        dialog = new AlertDialog.Builder(activity)
                .setView(activity.getLayoutInflater().inflate(resource, null))
                .create();
    }

    /**
     * Affiche le popup
     */
    public void show() {
        dialog.show();
        processAfterShow();
    }

    public abstract void processAfterShow();
}
