package fr.marlou.imfine.popup.help;

import fr.marlou.imfine.R;
import fr.marlou.imfine.SimulationActivity;
import fr.marlou.imfine.popup.Popup;

public class PopupHelp extends Popup<SimulationActivity> {

    public PopupHelp(SimulationActivity activity) {
        super(activity, R.layout.popup_help);
    }

    @Override
    public void processAfterShow() {}
}
