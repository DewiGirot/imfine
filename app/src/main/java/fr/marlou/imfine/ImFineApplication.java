package fr.marlou.imfine;

import android.app.Application;

import fr.marlou.imfine.sql.Database;
import fr.marlou.imfine.sql.SQLManager;

public class ImFineApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /*
        On vérifie qu'il n'y a aucun problème avec la base de données
         */
        SQLManager manager = SQLManager.getInstance(this);
        if(manager == null) {
            throw new RuntimeException("SQLManager problem (null)");
        }

        Database database = manager.getDatabase();
        if(database == null) {
            throw new RuntimeException("Database problem (null)");
        }

        if(database.getWritableDatabase() == null) {
            throw new RuntimeException("Database problem (writable null)");
        }

        if(database.getReadableDatabase() == null) {
            throw new RuntimeException("Database problem (readable null)");
        }
    }
}
