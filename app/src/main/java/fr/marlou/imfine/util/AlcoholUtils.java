package fr.marlou.imfine.util;

import java.util.HashMap;
import java.util.Map;

public class AlcoholUtils {

    //CE = Conducteur Expérimenté, JC = Jeune Conducteur
    public static final double RATE_CE = 0.5;
    public static final double RATE_JC = 0.2;

    //temps moyen d'élimination de l'alcool par heure
    public static final double DILUTION = 0.15;

    public static Map<Double, Long> getTime(double tauxAlcoolemie, boolean ventreVide) {

        int heure = 0;
        int minute = 0;
        int heureTauxLegalCE = 0;
        int minuteTauxLegalCE = 0;
        int heureTauxLegalJC = 0;
        int minuteTauxLegalJC = 0;

        double tauxAlcoolemieCE = tauxAlcoolemie;
        double tauxAlcoolemieJC = tauxAlcoolemie;

        double dilutionParMinute = DILUTION /60;

        //Temps pour arriver à 0.0 g/ml
        while(tauxAlcoolemie >= DILUTION) {
            tauxAlcoolemie -= DILUTION;
            heure += 1;
        }
        while(tauxAlcoolemie >= dilutionParMinute) {
            tauxAlcoolemie -= dilutionParMinute;
            minute += 1;
        }

        //Temps pour arriver à 0.5 g/ml
        while(tauxAlcoolemieCE >= dilutionParMinute && tauxAlcoolemieCE > RATE_CE) {
            tauxAlcoolemieCE -= dilutionParMinute;
            minuteTauxLegalCE += 1;
        }

        //Temps pour arriver à 0.2 g/ml
        while(tauxAlcoolemieJC >= dilutionParMinute && tauxAlcoolemieJC > RATE_JC) {
            tauxAlcoolemieJC -= dilutionParMinute;
            minuteTauxLegalJC += 1;
        }



        /* Pour éviter de compter les secondes et être sûr d'etre sobre à l'instant T on ajoute 1 minute
         * seulement et seulement si il reste des valeurs dans "tauxAlcoolemie"
         */
        if(minute !=0 && tauxAlcoolemie>0) {
            minute += 1;
        }

        if(tauxAlcoolemie > 0) {
            /* D'apr�s : https://www.bfmtv.com/static/nxt-bfmtv/evenement/auto/taux-alcool%C3%A9mie/index.html
             * il faut ajouter 30 min si à jeun sinon 1h de plus
             */
            if(ventreVide) {
                minute += 30;
                minuteTauxLegalCE += 30;
                minuteTauxLegalJC += 30;
            }else {
                heure+=1;
                heureTauxLegalCE+=1;
                heureTauxLegalJC+=1;
            }
        }


        //On converti les minutes afin que les minutes ne dépassent pas 60
        while(minute >= 60) {
            heure +=1;
            minute = minute - 60;
        }
        while(minuteTauxLegalCE >= 60) {
            minuteTauxLegalCE -= 60;
            heureTauxLegalCE +=1;
        }
        while(minuteTauxLegalJC >= 60) {
            minuteTauxLegalJC -= 60;
            heureTauxLegalJC +=1;
        }

        System.out.println(heure);

        Map<Double, Long> times = new HashMap<>();
        times.put(0.0, heure * 3600000L + minute * 60000L);
        times.put(0.5, heureTauxLegalCE * 3600000L + minuteTauxLegalCE * 60000L);
        times.put(0.2, heureTauxLegalJC * 3600000L + minuteTauxLegalJC * 60000L);

        return times;
    }
}
