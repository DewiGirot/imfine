package fr.marlou.imfine.util;

import android.content.Context;

import fr.marlou.imfine.R;

public class DateUtils {

    public static String getDateWithoutSeconds(Context context, long millis) {
        long millisInDay = 86400000;
        long millisInHour = 3600000;
        long millisInMinute = 60000;

        long days = millis / millisInDay;
        long daysDivisionResidueMillis = millis - days * millisInDay;

        long hours = daysDivisionResidueMillis / millisInHour;
        long hoursDivisionResidueMillis = daysDivisionResidueMillis - hours * millisInHour;

        long minutes = hoursDivisionResidueMillis / millisInMinute;

        String date = "";
        if(days > 0) {
            date += days + context.getResources().getString(R.string.day_show) + " ";
        }
        if (hours > 0) {
            date += hours + "h ";
        }
        if (minutes > 0) {
            date += minutes + "m";
        }
        return date;
    }

}
