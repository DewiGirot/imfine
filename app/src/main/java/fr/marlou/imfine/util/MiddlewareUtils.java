package fr.marlou.imfine.util;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

import fr.marlou.imfine.AuthActivity;
import fr.marlou.imfine.LoginActivity;
import fr.marlou.imfine.MainActivity;
import fr.marlou.imfine.service.error.APIErrorListener;
import fr.marlou.imfine.service.key.KeyManager;
import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.service.task.GetProfileTask;

public class MiddlewareUtils {

    /**
     * Gère la redirection de l'utilisateur si il n'a pas de clé de connexion valide
     *
     * @param activity
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static Profile redirect(AppCompatActivity activity) throws ExecutionException, InterruptedException {
        Class<? extends AppCompatActivity> classActivity = null;
        Profile profile = null;

        if(KeyManager.getInstance().getActualKey() != null) {
            // Envoie une requête pour récupérer le prfil associé à la clé
            profile = new GetProfileTask(APIErrorListener.EMPTY).execute().get();

            if(profile != null && activity instanceof AuthActivity) {
                classActivity = MainActivity.class;
            } else if(profile == null && !(activity instanceof AuthActivity)) {
                classActivity = LoginActivity.class;
            }

        } else if(!(activity instanceof AuthActivity)) {
            classActivity = LoginActivity.class;
        }

        if(classActivity != null) {
            // Lancement activité
            Intent intent = new Intent(activity, classActivity);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
        }
        return profile;
    }
}
