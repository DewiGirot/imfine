package fr.marlou.imfine;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import fr.marlou.imfine.alcohol.AlcoholManager;
import fr.marlou.imfine.listener.BottomNavigationClickListener;
import fr.marlou.imfine.service.model.Profile;
import fr.marlou.imfine.util.AlcoholUtils;
import fr.marlou.imfine.util.DateUtils;
import fr.marlou.imfine.util.MiddlewareUtils;

public class MainActivity extends CustomActivity {

    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            this.profile = MiddlewareUtils.redirect(this); // On récupère le profile avec la redirection
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigationView = this.findViewById(R.id.BottomNavMenu);
        navigationView.setOnItemSelectedListener(new BottomNavigationClickListener(navigationView, this, R.id.home));

        if(this.profile == null) return;

        // On affiche un message d'erreur si le profil n'est pas valide
        if(!profile.isComplete()) {
            findViewById(R.id.message_red).setVisibility(View.VISIBLE);
            return;
        }

        /*
        On calcule et affiche les données calculées grâce aux informations entrées dans SimulationActivity
         */
        double rate = profile.rateAlcoholism(AlcoholManager.getInstance().getCache());
        Map<Double, Long> times = AlcoholUtils.getTime(rate, false);

        this.setOrError(R.id.normal_time, times.get(0.5D));
        this.setOrError(R.id.young_time, times.get(0.2D));
        this.setOrError(R.id.clean_time, times.get(0.0D));
    }

    private void setOrError(int resource, long val) {
        ((TextView) findViewById(resource)).setText(val == 0L ? this.getResources().getString(R.string.no_data) :
                DateUtils.getDateWithoutSeconds(this, val));
    }
}